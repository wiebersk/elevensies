---
title: Documentation
subtitle: Documentation for elevensies shop
comments: false
---
# Elevensies Documentation

Welcome to the documentation for [Elevensies](elevensiesclothing.com) for Shopify administration.  This is intended to be a basic guide on how to use Shopify and the theme functionality to get the best results.  

## Shopify Resources

Shopify has a great wealth of documentation on their platform and should be leveraged first as it's the most up to date.  The resources below will help navigate custom implementation of the platform for the current Elevensies Theme.  

This is a large list and isn't required reading but a good reference to specific help articles which you may need.  This is not an exhaustive list.  

- [Selling from an Online Store](https://help.shopify.com/en/manual/sell-online/online-store)
- [Sales Channels (Different Apps you can list your items like Instagram, Pinterest)](https://help.shopify.com/en/manual/sell-online/online-sales-channels)
- [Product Overview](https://help.shopify.com/en/manual/products)
  - [Add or Update Products](https://help.shopify.com/en/manual/products/add-update-products)
  - [Collections](https://help.shopify.com/en/manual/products/collections)
  - [Managing Inventory](https://help.shopify.com/en/manual/products/inventory)
- [Shopify Analytics](https://help.shopify.com/en/manual/reports-and-analytics/shopify-reports) - help understand usage of the site
- [Marketing & Discounts](https://help.shopify.com/en/manual/promoting-marketing)
  - [Developing Marketing Plan](https://help.shopify.com/en/manual/promoting-marketing/developing-a-marketing-plan)
  - [Improve SEO](https://help.shopify.com/en/manual/promoting-marketing/seo) - how to influence how your site and products display on search engines (like Google, Bing).  
  - [Discount Codes](https://help.shopify.com/en/manual/promoting-marketing/discount-codes)
- [Shipping Info](https://help.shopify.com/en/manual/shipping/shopify-shipping)
- [Closing for Vacations](https://help.shopify.com/en/manual/sell-online/online-store/shopify-vacation-setting)


## Shopify Site Guidebook

### Critical Information

- Admin Site: https://elevensies.myshopify.com/admin
- Account: elevensiesclothing@gmail.com
- Navigation Addon: [Buddha Mega Menu](https://apps.shopify.com/buddha-mega-menu)

### Changing Logo

There are two locations in the Theme Editor that you need to change the logo:

- Header Options
  - Go to the [Theme Editor](https://elevensies.myshopify.com/admin/themes/153238027/editor) - this can be done by logging into the [Admin Site](https://elevensies.myshopify.com/admin) and clicking the edit button on the [homepage](elevensiesclothing.com)
  - Click on the Header Layout
  - Change the Logo
- Loader Image
  - Navigate to the top level Editor where the Sections and Theme Settings options are available.  
  - Click Theme Settings
  - Click Loader Image
  - Replace the Loader Image

### Change the Homepage Slider

The homepage slider can be changed out in the theme editor.  _You will need to play around with image sizes to get the best results across all platforms (mobile/desktop)._

- The slider should be on the Home Page editor view right below the Header Layout.  Click it (right now it says Elevensies Ready Made)
- Expand the content section and you can see all the details for each image slide.  You can edit the content here or remove a slide by clicking the `Remove content` slide.  
- You can add a slide by clicking the `Add image slide` link.  

### Change the Navigation

To change the navigation, you'll need to update the menu with the Mega Menu app.  

- Login to the [Admin Site](https://elevensies.myshopify.com/admin)
- Click on Apps
- Click on Mega Menu app in the list
- Once the menu loads, you can see the current navigation.  You can add to the top level or second level by clicking the Add Item link depending on where you want it and fill out the form.  

### Create a Pre-Order Product

I know you are moving towards ready to ship but thought I'd document this in the event you want to do a preorder again.  

1) Create a new Product Collection that ends in Pre-Order
2) Create Products for pre-order collection
   1) Tag the products with a tag of hidden - this hides them from the search results
   2) Choose the template (right hand side) of product.preorder
3) Kyle will have to create pictures with swatches (if needed).  

### Custom Product Option Dropdowns

Due to Shopify's limit on 100 total variant combination, I used the [Infinite Options](https://docs.theshoppad.com/article/303-infinite-options) add on.  This works by looking for a tag and adding options to pick based on the configuration in the Shopify Admin.  These are the tags I used for different customizable options:

- `suspenders` - Adds yes/no option for a suspenders dropdown
- `summer-sleeves` - Adds dropdown for Short/Sleeveless
- `snaps` - Adds dropdown for Snaps/No Snaps
- `pant length` - Adds dropdown for Shorts/Pants for pant length
- `sleeves` - Adds dropdown for Long/Short